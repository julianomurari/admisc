#ifndef NDPluginTrace_H
#define NDPluginTrace_H

#include <epicsTypes.h>

#include "NDPluginStdArrays.h"

#define NDPluginTraceTimeString             "TRACE_TIME"                  /* (asynFloat64ArrayIn, r/w) Array time points waveform in seconds */
#define NDPluginTraceTimePerPointString     "TRACE_TIME_PER_POINT"        /* (asynFloat64, r/w) Time point value in seconds */
#define NDPluginTraceTimeMsString           "TRACE_TIME_MS"               /* (asynFloat64ArrayIn, r/w) Array time points waveform in miliseconds */
#define NDPluginTraceTimePerPointMsString   "TRACE_TIME_PER_POINT_MS"     /* (asynFloat64, r/w) Time point value in miliseconds */
#define NDPluginTraceTimeUsString           "TRACE_TIME_US"               /* (asynFloat64ArrayIn, r/w) Array time points waveform in microseconds */
#define NDPluginTraceTimePerPointUsString   "TRACE_TIME_PER_POINT_US"     /* (asynFloat64, r/w) Time point value in microseconds */

/** Adds standard asynFloat64Array used for time stamps along side the NDStdArrays plugin data.
  * It supports 1-D NDArrays such as output from an ADC (digitizer).
  * Time and axis code was originally taken from NDPluginTimeSeries code. */
class epicsShareClass NDPluginTrace : public NDPluginStdArrays {
public:
    NDPluginTrace(const char *portName, int queueSize, int blockingCallbacks,
                      const char *NDArrayPort, int NDArrayAddr, int maxBuffers, size_t maxMemory,
                      int priority, int stackSize, int maxThreads=1);

    /* These methods override the virtual methods in the base class */
    void processCallbacks(NDArray *pArray);
    virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
protected:
    int NDPluginTraceTime;
    #define FIRST_NDPLUGIN_TRACE_PARAM NDPluginTraceTime
    int NDPluginTraceTimePerPoint;
    int NDPluginTraceTimeMs;
    int NDPluginTraceTimePerPointMs;
    int NDPluginTraceTimeUs;
    int NDPluginTraceTimePerPointUs;
private:
    /* These methods are just for this class */
    void createAxisArray();

    /* Actual time between points in input arrays */
    double timePerPoint_;
    int numTimePoints_;
    double *timeAxis_;
    double timePerPointMs_;
    double *timeAxisMs_;
    double timePerPointUs_;
    double *timeAxisUs_;
};

#endif
