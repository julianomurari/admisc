/*
 * NDPluginTrace.cpp
 *
 * Asyn driver for callbacks to standard asyn array interfaces for NDArray drivers.
 * This is commonly used for supplying NDStdArray EPICS waveform data records with
 * time stamps for each sample. Useful for plotting in OPI.
 *
 * Author: Hinko Kocevar
 *
 * Created September 28, 2018
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <epicsTypes.h>
#include <epicsMessageQueue.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsTime.h>
#include <iocsh.h>

#include <asynDriver.h>

#include <epicsExport.h>
#include "NDPluginDriver.h"
#include "NDPluginTrace.h"

static const char *driverName="NDPluginTrace";

/** Callback function that is called by the NDArray driver with new NDArray data.
  * It creates a time axis and calls base processCallbacks().
  * \param[in] pArray  The NDArray from the callback.
  */
void NDPluginTrace::processCallbacks(NDArray *pArray)
{
    static const char* functionName = "processCallbacks";

    /* This function generates the timestamps using the input sample time delta
     * and data array size.
     * It is called with the mutex already locked.
     */

    NDArrayInfo_t arrayInfo;
    pArray->getInfo(&arrayInfo);

    // re-create the time axis if needed
    if ((unsigned int)numTimePoints_ != arrayInfo.nElements) {
        numTimePoints_ = arrayInfo.nElements;
        asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER,
              "%s:%s: timePerPoint_ %g, numTimePoints_ %d\n",
              driverName, functionName, timePerPoint_, numTimePoints_);
        createAxisArray();
    }

    /* Call the base class method */
    NDPluginStdArrays::processCallbacks(pArray);
}

void NDPluginTrace::createAxisArray()
{
    // check for parameter sanity
    if (numTimePoints_ == 0 || timePerPoint_ == 0 || timePerPointMs_ == 0 || timePerPointUs_ == 0) {
        return;
    }

    // (re)allocate the time arrays
    if (timeAxis_) {
        free(timeAxis_);
    }
    timeAxis_ = (double *)calloc(numTimePoints_, sizeof(double));
    if (timeAxisMs_) {
        free(timeAxisMs_);
    }
    timeAxisMs_ = (double *)calloc(numTimePoints_, sizeof(double));
    if (timeAxisUs_) {
        free(timeAxisUs_);
    }
    timeAxisUs_ = (double *)calloc(numTimePoints_, sizeof(double));
    // fill in the time point values
    for (int i = 0; i < numTimePoints_; i++) {
        timeAxis_[i] = i * timePerPoint_;
        timeAxisMs_[i] = i * timePerPointMs_;
        timeAxisUs_[i] = i * timePerPointUs_;
    }
    /* Do callbacks so higher layers see any changes */
    doCallbacksFloat64Array(timeAxis_, numTimePoints_, NDPluginTraceTime, 0);
    doCallbacksFloat64Array(timeAxisMs_, numTimePoints_, NDPluginTraceTimeMs, 0);
    doCallbacksFloat64Array(timeAxisUs_, numTimePoints_, NDPluginTraceTimeUs, 0);
}

asynStatus NDPluginTrace::writeFloat64(asynUser *pasynUser, epicsFloat64 value)
{
    int function = pasynUser->reason;
    int signal;
    asynStatus status=asynSuccess;
    bool stat = true;
    static const char* functionName = "NDPluginTrace::writeFloat64";

    status = getAddress(pasynUser, &signal);
    if (status != asynSuccess) {
        return status;
    }

    /* Set the parameter in the parameter library. */
    stat = (setDoubleParam(signal, function, value) == asynSuccess) && stat;
    if (function == NDPluginTraceTimePerPoint) {
        // expected value is in seconds
        timePerPoint_ = value;
        // create milisecond time per point and update the milisecond parameter value
        timePerPointMs_ = timePerPoint_ * 1e3;
        stat = (setDoubleParam(signal, NDPluginTraceTimePerPointMs, timePerPointMs_) == asynSuccess) && stat;
        // create microsecond time per point and update the microsecond parameter value
        timePerPointUs_ = timePerPoint_ * 1e6;
        stat = (setDoubleParam(signal, NDPluginTraceTimePerPointUs, timePerPointUs_) == asynSuccess) && stat;
        createAxisArray();
    } else if (function < FIRST_NDPLUGIN_TRACE_PARAM) {
        stat = (NDPluginStdArrays::writeFloat64(pasynUser, value) == asynSuccess) && stat;
    }

    /* Do callbacks so higher layers see any changes */
    stat = (callParamCallbacks(signal) == asynSuccess) && stat;
    stat = (callParamCallbacks() == asynSuccess) && stat;

    if (!stat) {
    epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize,
        "%s: status=%d, function=%d, value=%f",
        functionName, status, function, value);
        status = asynError;
    } else {
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
            "%s: function=%d, signal=%d, value=%f\n",
            functionName, function, signal, value);
    }
    return(status);
}

/** Constructor for NDPluginTrace; all parameters are simply passed to NDPluginStdArrays::NDPluginStdArrays.
  * This plugin cannot block (ASYN_CANBLOCK=0) and is not multi-device (ASYN_MULTIDEVICE=0).
  * It allocates a maximum of 1 NDArray buffers for internal use.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] queueSize The number of NDArrays that the input queue for this plugin can hold when
  *            NDPluginDriverBlockingCallbacks=0.  Larger queues can decrease the number of dropped arrays,
  *            at the expense of more NDArray buffers being allocated from the underlying driver's NDArrayPool.
  * \param[in] blockingCallbacks Initial setting for the NDPluginDriverBlockingCallbacks flag.
  *            0=callbacks are queued and executed by the callback thread; 1 callbacks execute in the thread
  *            of the driver doing the callbacks.
  * \param[in] NDArrayPort Name of asyn port driver for initial source of NDArray callbacks.
  * \param[in] NDArrayAddr asyn port driver address for initial source of NDArray callbacks.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *      allowed to allocate. Set this to 0 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to 0 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] maxThreads The maximum number of threads this driver is allowed to use. If 0 then 1 will be used.
  */
NDPluginTrace::NDPluginTrace(const char *portName, int queueSize, int blockingCallbacks,
                                     const char *NDArrayPort, int NDArrayAddr, int maxBuffers, size_t maxMemory,
                                     int priority, int stackSize, int maxThreads)
    /* Invoke the base class constructor */
    : NDPluginStdArrays(portName, queueSize, blockingCallbacks,
                   NDArrayPort, NDArrayAddr, maxBuffers, maxMemory,
                   priority, stackSize, maxThreads),
    timePerPoint_(0), numTimePoints_(0), timeAxis_(NULL),
    timePerPointMs_(0), timeAxisMs_(NULL), timePerPointUs_(0), timeAxisUs_(NULL)
{
    //static const char *functionName = "NDPluginTrace";

    createParam(NDPluginTraceTimeString,         asynParamFloat64Array, &NDPluginTraceTime);
    createParam(NDPluginTraceTimePerPointString,      asynParamFloat64, &NDPluginTraceTimePerPoint);
    createParam(NDPluginTraceTimeMsString,       asynParamFloat64Array, &NDPluginTraceTimeMs);
    createParam(NDPluginTraceTimePerPointMsString,    asynParamFloat64, &NDPluginTraceTimePerPointMs);
    createParam(NDPluginTraceTimeUsString,       asynParamFloat64Array, &NDPluginTraceTimeUs);
    createParam(NDPluginTraceTimePerPointUsString,    asynParamFloat64, &NDPluginTraceTimePerPointUs);

    /* Set the plugin type string */
    setStringParam(NDPluginDriverPluginType, "NDPluginTrace");
}

/* Configuration routine.  Called directly, or from the iocsh function */
extern "C" int NDTraceConfigure(const char *portName, int queueSize, int blockingCallbacks,
                                    const char *NDArrayPort, int NDArrayAddr, int maxBuffers, size_t maxMemory,
                                    int priority, int stackSize, int maxThreads)
{
    NDPluginTrace *pPlugin = new NDPluginTrace(portName, queueSize, blockingCallbacks, NDArrayPort, NDArrayAddr,
                                                       maxBuffers, maxMemory, priority, stackSize, maxThreads);
    return pPlugin->start();
}


/* EPICS iocsh shell commands */
static const iocshArg initArg0 = { "portName",iocshArgString};
static const iocshArg initArg1 = { "frame queue size",iocshArgInt};
static const iocshArg initArg2 = { "blocking callbacks",iocshArgInt};
static const iocshArg initArg3 = { "NDArrayPort",iocshArgString};
static const iocshArg initArg4 = { "NDArrayAddr",iocshArgInt};
static const iocshArg initArg5 = { "maxBuffers",iocshArgInt};
static const iocshArg initArg6 = { "maxMemory",iocshArgInt};
static const iocshArg initArg7 = { "priority",iocshArgInt};
static const iocshArg initArg8 = { "stack size",iocshArgInt};
static const iocshArg initArg9 = { "# threads",iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
                                            &initArg1,
                                            &initArg2,
                                            &initArg3,
                                            &initArg4,
                                            &initArg5,
                                            &initArg6,
                                            &initArg7,
                                            &initArg8,
                                            &initArg9};
static const iocshFuncDef initFuncDef = {"NDTraceConfigure",10,initArgs};
static void initCallFunc(const iocshArgBuf *args)
{
    NDTraceConfigure(args[0].sval, args[1].ival, args[2].ival,
                            args[3].sval, args[4].ival, args[5].ival,
                            args[6].ival, args[7].ival, args[8].ival,
                            args[9].ival);
}

extern "C" void NDTraceRegister(void)
{
    iocshRegister(&initFuncDef,initCallFunc);
}

extern "C" {
epicsExportRegistrar(NDTraceRegister);
}
